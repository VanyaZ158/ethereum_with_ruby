class BookEditionsController < ApplicationController
  before_action :find_book_edition, only: [:show]

  def index
    @book_editions = BookEdition.order(:id).page(params[:page])
  end

  def show; end

  def new
    @book_edition = BookEdition.new
  end

  private

  def find_book_edition
    @book_edition = BookEdition.find(params[:id])
  end
end
