Rails.application.routes.draw do
  root 'book_editions#index'

  resources :book_editions, except: [:destroy]
end
